//
//  MapTestApp.swift
//  MapTest
//
//  Created by Администратор on 09.12.2020.
//

import SwiftUI

@main
struct MapTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
